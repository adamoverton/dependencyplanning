// DependencyPlanning.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>

#include <yaml.h>

#define crlf "\r\n"
using namespace std;


class task
{
    // These are the properties directly from the YAML
    string _name;
    unsigned int _coresRequired;
    unsigned int _executionTime;
    vector<string> _parents;

public:
    unsigned int worstChildDuration;

    // This is the worst case chain reaction of duration
    task(string Name, unsigned int cores, unsigned int executionTime, string parents) :
        _name(Name), _coresRequired(cores), _executionTime(executionTime)
    {
        // parse comma separated list of parents into map
        stringstream ss(parents);

        while (ss.good())
        {
            string substr;
            getline(ss, substr, ',');
            substr.erase(0, substr.find_first_not_of(" \t\n\r\f\v"));
            if (substr.size() > 0)
            {
                _parents.push_back(substr);
            }
        }
    }

    string Name() const 
    {
        return _name;
    }

    unsigned int CoresRequired() const 
    {
        return _coresRequired;
    }

    unsigned int ExecutionTime() const 
    {
        return _executionTime;
    }

    const vector<string> &Parents() const 
    {
        return _parents;
    }

};

class CompareTasks
{
public:
    bool operator() (task t1, task t2)
    {
        return t1.worstChildDuration < t2.worstChildDuration;
    }
};

bool FCompareTasks(task t1, task t2)
{
    return t1.worstChildDuration < t2.worstChildDuration;
}

class planNode
{
public:
    unsigned int startTime; // in ticks
    unsigned int duration;
    string taskName;
    string computeName;
};

class computeResource
{
    string _name;
    unsigned int _cores;
    vector<planNode> _plans;

public:
    computeResource(string Name, unsigned int Cores) :
        _name(Name), _cores(Cores)
    {
    }

    const vector<planNode> & Plans() const
    {
        return _plans;
    }


    //
    // This currently searches the schedule list for an open slot linearly, and 
    // we do for every schedule list every time we want to schedule a task. 
    // From a time complexity, this is O(horribad).
    //
    // We would prefer to use a method where finding free slots was not dependent
    // on how many things we've already scheduled. A separate free slot list would 
    // do the trick, as the free slots filled up it would just point to the end
    // and we'd go right there. I ran out of time.
    //
    // Another small optimization would be to at least pass in what time we need to 
    // beat, so if we can't beat the time, we just stop searching.
    //
    tuple<unsigned int, vector<planNode>::iterator> FindEarliestSlotAfterTime(unsigned int startTime, unsigned int executionTime)
    {
        vector<planNode>::iterator it = _plans.begin();
        unsigned int scheduledTime = startTime; // with no plan, fall through with the correct start time

        if (it != _plans.end())
        {
            vector<planNode>::iterator itLast = it;
            it++;

            // if only one item in the plan, start after it
            scheduledTime = max(scheduledTime, itLast->startTime + itLast->duration);

            while (it != _plans.end())
            {
                unsigned int lastEnd = itLast->startTime + itLast->duration;

                // Previous has to end before the start AND
                // next start has to start after the task start + task duration
                if (lastEnd >= startTime && startTime + executionTime < it->startTime)
                {
                    scheduledTime = max(scheduledTime, it->startTime + it->duration);
                    break;
                }
                else
                {
                    itLast = it;
                    it++;
                }
            }
        }
        // If we reach _plan.end, we will schedule it at whatever time we want
        return make_tuple(scheduledTime, it);
    }
    void ScheduleTask(task t, tuple<unsigned int, vector<planNode>::iterator> tup)
    {
        planNode pn;
        pn.startTime = get<0>(tup);
        pn.duration = t.ExecutionTime();
        pn.taskName = t.Name();
        pn.computeName = _name;

        _plans.insert(get<1>(tup), pn);
    }
    unsigned int EndTime()
    {
        return (_plans.size() == 0) ? 0 : _plans.back().duration + _plans.back().startTime;
    }

    string Name() const { return _name;  }
    unsigned int Cores() const { return _cores; }
};

class planner
{
    vector<task> _tasks;
    map<string, unsigned int> _originalIndexes; // map of name to index in _tasks
    priority_queue<task, vector<task>, CompareTasks> _queue; // this is the prioritizes list of tasks, in order of long pole
    vector<computeResource> _resources; // this is the static list of compute resources

    // This parallel structure will let us know when a task is due to be completed
    // same indexes as _tasks
    vector<unsigned int> _completionTimes;

    //
    // Recursively add our worst duration to any tasks 
    // 
    void AddDurations(unsigned int c)
    {
        for (string pstr: _tasks[c].Parents())
        {
            unsigned int p = _originalIndexes[pstr];

            // the child time for our parent is our own children plus our own time
            unsigned int childTime = _tasks[c].worstChildDuration + _tasks[c].ExecutionTime();

            // If the parent doesn't already have a worse case, update it, and all it's parents recursively
            if (_tasks[p].worstChildDuration < childTime)
            {
                _tasks[p].worstChildDuration = childTime;
                AddDurations(p);
            }
        }
    }

    void CalculateWorstDurations()
    {
        //
        // ASSUMPTION: I'm assuming that when we get tasks, we never get a task that has a parent
        // that isn't defined yet. This allows me to work from the end back and if an item already
        // has a worstChildDuration, because AddDurations is recursive, it's already taken care of
        // its parents, so I don't have to do it again. This reduced the complexity of calculating
        // these durations from O(n^2)
        // 
        for (int i = (int)_tasks.size() - 1; i >= 0; i--)
        {
            if (_tasks[i].worstChildDuration == 0)
            {
                AddDurations((unsigned int)i);
            }
        }
    }

    void LoadTasksFromYaml(YAML::Node yaml)
    {
        for (YAML::iterator it = yaml.begin(); it != yaml.end(); it++)
        {
            // task name
            string taskName = it->first.as<string>();
            // cout << taskName << endl;

            // cores
            unsigned int cores = yaml[taskName]["cores_required"].as<unsigned int>();
            // cout << " cores_required: " << cores << endl;

            // execution time
            unsigned int executionTime = yaml[taskName]["execution_time"].as<unsigned int>();
            // cout << " execution_time: " << executionTime << endl;

            string parents;
            if (yaml[it->first.as<string>()]["parent_tasks"].IsScalar())
            {
                parents = yaml[it->first.as<string>()]["parent_tasks"].as<string>();
                // cout << " parents: " << parents << endl;
            }

            // create the task
            task t(taskName, cores, executionTime, parents);

            // Add it to our storage
            unsigned int index = (unsigned int)_tasks.size();
            _tasks.push_back(t);
            _completionTimes.push_back(0);
            _originalIndexes[taskName] = index;

        }

        // set up all the worst durations
        CalculateWorstDurations();

        // make a priority queue from these elements.
        // I could just do make_heap, but the pq is easier,
        // and at the time of writing, I don't know if I will need the initial order again
        for (int i = 0; i < (int)_tasks.size(); i++)
        {
            _queue.push(_tasks[i]);
        }
    }

    void LoadComputeFromYaml(YAML::Node yaml)
    {
        for (YAML::iterator it = yaml.begin(); it != yaml.end(); it++)
        {
            // compute name
            string computeName = it->first.as<string>();
            // cout << computeName << endl;

            // cores
            unsigned int cores = it->second.as<unsigned int>();
            // cout << cores << endl;

            computeResource cr(computeName, cores);

            _resources.push_back(cr);
        }
    }

    unsigned int EarliestTimeForTask(task t)
    {
        unsigned int earliest = 0;

        for (string p : t.Parents())
        {
            earliest = max(earliest, _completionTimes[_originalIndexes[p]]);
        }
        return earliest;
    }

public:
    void LoadFromYaml(YAML::Node yamlTasks, YAML::Node yamlCompute)
    {
        LoadTasksFromYaml(yamlTasks);
        LoadComputeFromYaml(yamlCompute);
    }

    void MakePlan()
    {
        // Pull each task off based on priority and schedule it for the earliest available time
        while (!_queue.empty())
        {
            const task t = _queue.top();

            int pick = -1;
            tuple<unsigned int, vector<planNode>::iterator> pickTup;

            // Just search linearly through the compute resources for the best match because it is simple and I'm short on time
            // One simple optimization would be to sort the compute resources based on cores, and then have a small array to 
            // indicate where the first qualifying resource would be. So if your compute resources had 1,1,1,2,4,6 cores,
            // the array would be 0,3,4,4,5,5 to indicate the indexe to start at to  search for the corresponding
            // cores for          1,2,3,4,5,6 so you could start this loop with for (i = coresRequiredIndex[cores];)
            for (int i = 0; i < (int)_resources.size(); i++)
            {
                bool coresOK = (t.CoresRequired() <= _resources[i].Cores());

                if (!coresOK)
                    continue;

                unsigned int noSooner = EarliestTimeForTask(t);

                // gotta try to put it somewhere
                if (pick == -1)
                {
                    pick = i;
                    pickTup = _resources[pick].FindEarliestSlotAfterTime(noSooner, t.ExecutionTime());
                    continue;
                } 

                // Pick should be valid now
                auto possiblePlan = _resources[i].FindEarliestSlotAfterTime(noSooner, t.ExecutionTime());

                if (get<0>(possiblePlan) < get<0>(pickTup))
                {
                    pick = i;
                    pickTup = possiblePlan;
                }
                else if (get<0>(possiblePlan) == get<0>(pickTup) && _resources[i].Cores() < _resources[pick].Cores())
                {
                    // We can be done at the same time with fewer cores
                    pick = i;
                    pickTup = possiblePlan;
                }
            }

            if (pick == -1)
            {
                cerr << "No valid compute resource for task: " << t.Name() << endl;
                return;
            }

            // Provide the output
            // cout << t.Name() << ": " << _resources[pick].Name() << endl;

            // update our structures
            _resources[pick].ScheduleTask(t, pickTup);
            _completionTimes[_originalIndexes[t.Name()]] = get<0>(pickTup) + t.ExecutionTime();
            _queue.pop();
        }
    }

    // Print out the stored plan
    void PrintPlan()
    {
        vector<planNode> allPlans;

        //
        // I could do this in place by having a vector of iterators, and then advancing
        // them in parallel, always doing the lowest one, but the computational and code complexity
        // makes me sad, so I'm going to use up space by putting all the nodes in a vector and then
        // sorting it
        //
        for (int i = 0; i < (int)_resources.size(); i++)
        {
            for (int j = 0; j < (int)_resources[i].Plans().size(); j++)
            {
                allPlans.push_back(_resources[i].Plans()[j]);
            }
        }

        // I'm just going to use this lambda I got from the internet rather than creating a compare function
        sort(allPlans.begin(), allPlans.end(), [](const auto& lhs, const auto& rhs) {
            return lhs.startTime < rhs.startTime;
        });

        for (int i = 0; i < (int)allPlans.size(); i++)
        { 
            cout << allPlans[i].taskName << ": " << allPlans[i].computeName << endl;
        }

        unsigned int totalTicks = 0;
        for (auto r : _resources)
        {
            totalTicks = max(totalTicks, r.EndTime());
        }
        cout << "Total time: " << totalTicks << endl;
    }
};

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        cout << "Usage: dp.exe <yaml_tasks_file> <yaml_cores_file>" << endl;
        return 0;
    }

    // read the tasks
    YAML::Node yamlTasks = YAML::LoadFile(argv[1]);
    YAML::Node yamlCompute = YAML::LoadFile(argv[2]);

    //cout << (yamlTasks.IsMap() ? "true" : "false");
    //cout << (yamlTasks["task1"]["cores_required"].as<int>());

    planner plan;

    plan.LoadFromYaml(yamlTasks, yamlCompute);
    plan.MakePlan();
    plan.PrintPlan();

    return 0;
}

